"""
rescan - Reverse port scanner

Auto responder:
python examples/scan_responder.py

Firewall rulez for router:
iptables -t nat -I zone_wan_prerouting 1 -m u32 --u32 "0&0x00FF0000=0x000C0000 && 4&0xFFFF0000=0x13380000:0x13380000" -j DNAT --to 192.168.1.112
iptables -t nat -I PREROUTING 1 -m u32 --u32 "4&0xFFFF0000=0x13380000:0x13380000" -j DNAT --to 192.168.1.112
"""
import threading
import time
import queue
import socket
import random
import sys
import logging
import argparse

from pypacker.layer12 import ethernet
from pypacker.layer3 import ip
from pypacker.layer4 import tcp, udp
from pypacker import psocket
from pypacker import utils

logger = logging.getLogger("rescan")
logger.setLevel(logging.INFO)
#logger.setLevel(logging.DEBUG)

logger_streamhandler = logging.StreamHandler()
logger_formatter = logging.Formatter("%(message)s")
logger_streamhandler.setFormatter(logger_formatter)

logger.addHandler(logger_streamhandler)

BANNER = """
 ____      ____
|  _ \ ___/ ___|  ___ __ _ _ __
| |_) / _ \___ \ / __/ _` | '_ \\
|  _ <  __/___) | (_| (_| | | | |
|_| \_\___|____/ \___\__,_|_| |_|
"""
parser = argparse.ArgumentParser(
	description=BANNER + "\nReScan - the reverse scanner",
	formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-i", "--interface", type=str, help="Interface", required=True)
parser.add_argument("-f", "--filename", type=str, help="File to save result to. Otherwise stdout is used",
	required=False, default=None)

args = parser.parse_args()

IFACE_NAME = args.interface
FILENAME_SAVE = args.filename

try:
	MAC_SRC = utils.get_mac_for_iface(IFACE_NAME)
	IP_SRC = utils.get_ipv4_for_iface(IFACE_NAME)
except Exception as ex:
	logger.warning("Could not retrieve interface information for %s", IFACE_NAME)
	print(ex)
	sys.exit(1)

logger.info("Using interface %s: %s / %s", IFACE_NAME, IP_SRC, MAC_SRC)

try:
	IP_GW = utils.get_gwip_for_iface(IFACE_NAME)
	MAC_DST = utils.get_arp_cache_entry(IP_GW)
except:
	logger.warning("Could not retrieve gateway information for %s", IFACE_NAME)
	sys.exit(1)

logger.info("Gateway address: %s / %s", IP_GW, MAC_DST)
HOSTNAME_DST = "redirm2.hopto.org"

try:
	IP_DST = socket.gethostbyname(HOSTNAME_DST)
except:
	logger.warning("Could not resolve %s. Are you online? Please retry after a while or check original repo"
			"for address-updates")
	sys.exit(1)
logger.info("Target IP: %s", IP_DST)

#sys.exit(0)
PORT_SRC = (int)(random.random() * 0xFFFF)
logger.debug("Source port: 0x%X", PORT_SRC)
SCATTER = (int)(random.random() * 0xFFFF)
logger.debug("Scatter: 0x%X", SCATTER)

sock_sendrecv = psocket.SocketHndl(iface_name=IFACE_NAME, buffersize_send=100000, timeout=5)
open_ports = {"TCP": set(), "UDP": set()}


def filter_pkt(pkt):
	# TCP or UDP
	pkt_transport = pkt.upper_layer.upper_layer

	if pkt_transport is None or not pkt_transport.bin().endswith(b"Pypacker"):
		return False

	try:
		tname = pkt_transport.__class__.__name__
		#print(tname)
		#print(pkt_transport.sport)
		open_ports[tname].add(pkt_transport.sport)
	except KeyError:
		# unknown proto
		pass
	except Exception as ex:
		print(ex)
	return True


def rcv_cycler(sock):
	while True:
		try:
			sock.recvp(filter_match_recv=filter_pkt,
				lowest_layer=ethernet.Ethernet,
				max_amount=1)
		except socket.timeout:
			# no packets received, don't mind
			pass
		except socket.error as ex:
			logger.debug("Stopping receive cycler")
			#print(ex)
			break
		except Exception as ex:
			print(ex)
			pass


thread_rcv = threading.Thread(target=rcv_cycler, args=[sock_sendrecv])
thread_rcv.start()

logger.info("Starting to scan for open ports")

socket_send = sock_sendrecv.send

pkt_eth_ip_transport = ethernet.Ethernet(dst_s=MAC_DST, src_s=MAC_SRC) +\
	ip.IP(src_s=IP_SRC, dst_s=IP_DST, tos=0x0C, id=0x1338)

pkt_send_tcp = pkt_eth_ip_transport
tcp_layer = tcp.TCP(sport=PORT_SRC, dport=0, seq=23142)
pkt_send_tcp.upper_layer.upper_layer = tcp_layer
pkt_send_tcp_bin = pkt_send_tcp.bin


for port in range(0, 0xFFFF + 1):
	port_send = port ^ SCATTER

	if port & 0xFF == 0xFF:
		print("\rTCP-Scan progress: %3d%%, %5d ports found" %
			(port * 100 / 0xFFFF, len(open_ports["TCP"])), end="")

	tcp_layer.dport = port_send
	tcp_layer.sport ^= port_send
	socket_send(pkt_send_tcp_bin())

print()

pkt_send_udp = pkt_eth_ip_transport
udp_layer = udp.UDP(sport=PORT_SRC, dport=0, seq=23142)
pkt_send_udp.upper_layer.upper_layer = udp_layer
pkt_send_udp_bin = pkt_send_udp.bin

for port in range(0, 0xFFFF + 1):
	port_send = port ^ SCATTER

	if port & 0xFF == 0xFF:
		print("\rUDP-Scan progress: %3d%%, %5d ports found" %
			(port * 100 / 0xFFFF, len(open_ports["UDP"])), end="")

	udp_layer.dport = port_send
	udp_layer.sport ^= port_send
	socket_send(pkt_send_udp_bin())

print()
logger.info("Waiting some seconds to receive delayed responses")
time.sleep(5)
sock_sendrecv.close()


write_fct = lambda line: print(line)
fd_out = None

if FILENAME_SAVE is not None:
	logger.debug("Writing result to %s", FILENAME_SAVE)
	fd_out = open(FILENAME_SAVE, "w")
	write_fct = lambda line: fd_out.write(line + "\n")

for key, ports in open_ports.items():
	for port in ports:
		write_fct("%s:%d" % (key, port))

if fd_out is not None:
	fd_out.close()
