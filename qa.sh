function msg {
	echo ""
	echo ">>> $1"
}

if [ -z "$1" ]; then
	echo "usage: $0 [stable | dev]"
exit 1
fi

case "$1" in
"stable")
	msg "Any TODOs open?"
	grep -ir "TODO "

	msg "replacing version numbers"
	sed -r -i "s/version=\".+\"/version=\"$VERSION\"/g" setup.py
	#sed -r -i "s/version = '.+'/version = '$VERSION'/g; s/release = '.+'/release = '$VERSION'/g" doc_sphinx_generated/conf.py

	msg "searching for not disabled debug output"
	grep -ir -R "^[^#]*logger.debug" *

	msg "doing style checks"
	msg "PEP8"
	pep8  --config=./qa_config.txt ./

	msg "flake8"
	flake8 --config=./qa_config.txt ./pypacker

	#msg "Pylint"
	#pylint --rcfile=./.pylintrc $pydir/*.py

	#prospector --profile .landscape.yaml

	msg "searching untracked NOT ignored files... did you forget to add anything?"
	# Show untracked files
	#git ls-files --others --exclude-from=.git/.gitignore
	# Show only ignored files
	#git ls-files --ignored  --exclude-from=.git/.gitignore
	# --exclude-standard:
	# Add the standard Git exclusions: .git/info/exclude, .gitignore in each directory, and the user’s global exclusion file.
	#git ls-files --others --exclude-standard | grep -v -P ".pyc|doc_sphinx_generated|.idea|dist"
	git ls-files --others --exclude-from=.git/.gitignore

	msg "python string instead of bytes? (no output=OK)"
	grep -ir -Po "\"\ds\", *\".+"

	msg "set(...) instead of {...}? (still needed for list which need to be made unique)"
	grep -ir -Po " set\([^)]" | grep ".py:" | uniq
	# show set usages of form {...}
	#grep -ir -Po " {[^\:]+}" | grep ".py:" | uniq

	msg "Lower case hex?"
	grep -r -P "0x[0-9]*?[a-f]+" *.py

	msg "Old style unpack like unpack('H', value)? (non precompiled structs)"
	grep -ir -P "unpack\([\"\']" | grep  -P ".py:"

	msg "if everything is OK call: git push -u origin master --tags; python setup.py sdist upload"
;;
"dev")
	msg "changing debug level to DEBUG"
	sed -r -i "s/# (logger.setLevel\(logging.DEBUG\))/\1/;s/^(logger.setLevel\(logging.WARNING\))/# \1/g" pypacker/pypacker.py
;;
esac
