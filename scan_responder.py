import threading
import time
import queue
import subprocess

from pypacker.layer12 import ethernet
from pypacker.layer3 import ip
from pypacker.layer4 import tcp, udp
from pypacker import psocket
from pypacker import interceptor

IFACE_NAME	= "wlp2s0"
MAC_IFACE_LOCAL	= "00:1e:65:6c:25:a2"
MAC_ROUTER	= "00:26:4d:e9:e4:4e"

# iptables -I INPUT 1 -j NFQUEUE --queue-balance 0:2
CMD_IPTABLES_START	= "iptables -I INPUT 1 -j NFQUEUE --queue-balance 0:2"
CMD_IPTABLES_END	= "iptables -D INPUT 1"

# WAN incoming scan -> server, TOS is changed by intermediate routers
#iptables -t nat -I zone_wan_prerouting 1 -m u32 --u32 "4&0xFFFF0000=0x13380000:0x13380000" -j DNAT --to 192.168.1.112

sock_send = psocket.SocketHndl(iface_name=IFACE_NAME)
# assume only IPv4
bts_l2 = ethernet.Ethernet(dst_s=MAC_ROUTER, src_s=MAC_IFACE_LOCAL, type=ethernet.ETH_TYPE_IP).bin()


def verdict_cb(ll_data, ll_proto_id, data, ctx):
	ip1 = ip.IP(data)

	if ip1.tos == 0x0C and ip1.id == 0x1338:
		print("got a matching packet: %s", ip1)
		try:
			ip1.reverse_all_address()
			ip1.highest_layer.body_bytes += b"Pypacker"
			sock_send.send(bts_l2 + ip1.bin())
		except Exception as ex:
			print(ex)
		return data, interceptor.NF_DROP

	# We respond to this packet ourself
	return data, interceptor.NF_DROP


print(subprocess.getoutput(CMD_IPTABLES_START))
ictor = interceptor.Interceptor()
ictor.start(verdict_cb, queue_ids=[0, 1, 2])


try:
	time.sleep(9999)
except KeyboardInterrupt:
	pass

sock_send.close()
ictor.stop()
print(subprocess.getoutput(CMD_IPTABLES_END))
