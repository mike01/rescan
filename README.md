[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](LICENSE)

# General information
This is rescan, a reverse port scanner allowing to determine open ports in the reverse direction.
Normal port scanners allow you to scan a target host from an outer view e.g. to find running
services on a server. Rescan allows you from point of an inner-network view to find ports which
are allowed by firewalls.

``
[Host w/ rescan] <-> [DMZ/Firewall] <-> [Internet]
``

## Prerequisites
- Linux based system
- Python 3.x (CPython, Pypy)

## Installation
- This is an on-the-got installation, just one directory, NO system pollution!
* Clone this project
* cd rescan
* ./get_dependencies.sh

## Usage examples
Start scan via ``python rescan.py interface`` where interface is your outgoing interface
for sending out packets (needs root).

```
host /# python rescan.py -i wlp3s0 -f result.txt
Using interface wlp3s0: 10.51.212.12 / a0:88:34:75:22:12
Gateway address: 10.22.1.254 / 00:00:00:00:00:00
Target IP: 42.217.204.119
Starting to scan for open ports
TCP-Scan progress: 100%, 27081 ports found
UDP-Scan progress: 100%, 15353 ports found
Waiting some seconds to receive delayed responses
```
## Performance
Scanning all TCP and UDP ports takes under a minute on a 1,6 GHz machine.

## Misc
If you like this project take a moment to give a star :D