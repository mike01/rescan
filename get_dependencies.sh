pypacker_repo_local="pypacker_repo"
pypacker_dir="pypacker"

if [ ! -d $pypacker_repo_local ]; then
	echo "No local pypacker repo found, cloning"
	git clone https://gitlab.com/mike01/pypacker.git $pypacker_repo_local
	cp -r $pypacker_repo_local/$pypacker_dir .
else
	echo "Updating local pypacker"
	pushd $pypacker_repo_local
	git pull
	popd

	rm -rf $pypacker_dir
	cp -r $pypacker_repo_local"/"$pypacker_dir ./
fi
