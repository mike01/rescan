setup(name="rescan",
	version="",
	author="Michael Stahn",
	author_email="michael.stahn.42@gmail.com",
	url="https://gitlab.com/mike01/rescan",
	description="rescan: the reverse portscanner",
	license="GPLv2",
	packages=[
	],
	classifiers=[
		"Development Status :: 6 - Mature",
		"Intended Audience :: Developers",
		"License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
		"Natural Language :: English",
		"Programming Language :: Python :: 3.3",
		"Programming Language :: Python :: 3.4",
		"Programming Language :: Python :: 3.5",
		"Programming Language :: Python :: 3.6",
		"Programming Language :: Python :: Implementation :: CPython",
		"Programming Language :: Python :: Implementation :: PyPy"
	],
	python_requires=">=3.3.*"
)
